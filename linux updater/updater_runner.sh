#!/bin/bash
install_unzip() {
    command_not_found_handle() {
        echo "install using yum"
        yum -y install unzip
        return 127
    }
    echo "installing with apt-get"
    apt-get install unzip -y
}


command_not_found_handle() {
    echo "unzip package not found"
    return 127
}

unzip > /dev/null
unzip_exit_status=$?

if [[ $unzip_exit_status == 127 ]]; then
    install_unzip
fi
unset command_not_found_handle

set -e

path=$(pwd)

service_name=ipes-${path##*\/}
printf "Updating service $service_name \n\n"

curl https://bitbucket.org/soadevelopers/ipes-connectors-installers/raw/master/connector.jar --output new_connector.jar -s

command_not_found_handle() {
    exec >/dev/tty
    echo "No systemctl found!"
    return 127
}

error_stopping_service() {
    echo "Error stopping service..."
    sudo rm new_connector.jar
    exit 1
}

stop_with_service() {
    echo "Stopping service using 'service'"
    service_status_message=$(service $service_name stop 2>&1 >/dev/null)
    service_status_code=$?
    if [[ service_status_code -ne 0 ]]; then
        if [[ $service_status_message != *"running"* ]]; then
            echo uno
            error_stopping_service
        fi
    fi
}

set +e
echo "Stopping service using 'systemctl'"
systemctl stop $service_name
systemctl_exit_status=$?

unset command_not_found_handle
echo quase finall
if [[ $systemctl_exit_status == 127 ]]; then
    stop_with_service
elif [[ $systemctl_exit_status != 0 ]]; then
    error_stopping_service
fi

echo removing old connector...
mv connector.jar old_connector.jar


set -e
echo moving new connector....
mv new_connector.jar connector.jar

if [[ $systemctl_exit_status != 0 ]]; then
    service $service_name start
else
    systemctl start $service_name
fi

echo fim
exit 0