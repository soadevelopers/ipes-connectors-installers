#!/bin/bash

SERVICE_DESTINATION=/opt/ipes/connector
SERVICE_NAME=ipes-connector

version_link=https://bitbucket.org/soadevelopers/ipes-connectors-installers/raw/master/version.json
updater_link=https://bitbucket.org/soadevelopers/ipes-connectors-installers/raw/master/linux%20updater/updater_runner.sh

if [ "$EUID" -ne 0 ]
  then echo "Para esse script de instalação, rode como usuário root"
  exit 1
fi


interrupt_installation() {
    printf "Deseja continuar a instalação? [s/N]  "
    option='N'
    read option

    if [[ $option != 's' ]]; then
        echo "Encerrando o instalador..."
        exit 1
    fi
}


get_values_from_user() {
    if [[ $ask_values == true ]]; then
        printf "Complete as seguintes informações\n\n"
    fi
    for key in "${properties_order[@]}";
    do
        key_text=$key"-text"
        key_default=$key"-default"
        key_comment=$key"-comment"
        key_read=$key"-read"
        if [[ ${properties["$key"]} == false ]]; then
            while [[ ${properties["$key"]} == false ]]; do
                printf "${properties[$key_text]}";
                if [[ ${properties[$key_default]} ]]; then
                    printf " [${properties[$key_default]}]"
                fi
                printf ": "
                read properties["$key"]
                if [[ $key == "spring.beans.location" ]]; then
                    if [[ ${properties["$key"]} == 1 ]]; then
                        properties["$key"]="classpath:/beans_PEC3.xml"
                    fi
                    if [[ ${properties["$key"]} == 2 ]]; then
                        properties["$key"]="classpath:/beans_PEC4.xml"
                    fi
                    if [[ ${properties["$key"]} == 3 ]]; then
                        properties["$key"]="classpath:/beans_CDS.xml"
                    fi
                    if [[ ${properties["$key"]} == 4 ]]; then
                        properties["$key"]="classpath:/beans_GSEA.xml"
                    fi
                    if [[ ${properties["$key"]} == 5 ]]; then
                        properties["$key"]="classpath:/beans_SISUPA.xml"
                    fi
                fi
                if [[ ${properties[$key]} = "" ]]; then
                    if [[ ${properties[$key_default]} = "" ]]; then
                        if [[ $key = "model.cds.municipio.ibgeNotIn" ]]; then
                            properties[$key]=""
                        else
                            properties[$key]=false
                            continue
                        fi
                    else
                        properties["$key"]=${properties[$key_default]}
                    fi
                fi

                echo "" >> $properties_file
                if [[ ${properties[$key_comment]} ]]; then
                    echo "# ${properties[$key_comment]}" >> $properties_file
                fi
                if [[ $key = "model.cds.municipio.ibgeNotIn" ]]; then
                    ibgeCodes=${properties[$key]}
                    if [[ ${properties[$key]} != "" ]]; then
                        newIbgeCodes=""
                        for i in $(echo $ibgeCodes | tr "," "\n")
                        do
                            newIbgeCodes+="'$i',"
                        done
                        properties[$key]="${newIbgeCodes%?}"
                    fi
                fi
                echo "$key=${properties[$key]}" >> $properties_file
            done
        else
            if [[ ${properties[$key_read]} = "" ]]; then
                if [[ ${properties[$key_comment]} ]]; then
                    echo "# ${properties[$key_comment]}" >> $properties_file
                fi
                echo "$key=${properties[$key]}" >> $properties_file
            fi
        fi
    done
    sed -i '/^$/d' $properties_file
}

get_values_from_file() {
    sed -i '/agent.custodian.id.sequencial/d' $properties_file
    sed -i '/extractor.sql.maxDate/d' $properties_file
    sed -i '/extractor.sql.minDate/d' $properties_file
    sed -i '/Intervalo de tempo para considerar na extração de RAC/d' $properties_file
    sed -i '/classpath:\/beans.xml/d' $properties_file
    sed -i '/classpath:\/beans_PEP.xml/d' $properties_file
    sed -i '/Número sequencial identificador da instância do connector em uma máquina/d' $properties_file


    while IFS= read -r line; do
        if [[ -z $line ]]; then
            continue
        fi
        key=(${line%=*})
        key_read=$key"-read"
        value=(${line#*=})
        properties[$key]="$value"
        properties[$key_read]=true
    done < "$properties_file"
}


properties_file="" 
jar_file="" 
declare -A properties
properties=(
    ["agent.custodian.name-text"]="Informe o nome da sua organização"
    ["agent.custodian.name-comment"]="Organização parceira com custódia/guarda do registro"
    ["agent.custodian.name"]="FUNDACAO ESTATAL SAUDE DA FAMILIA DA BAHIA FESF"

    ["agent.custodian.id.extension-text"]="Informe o OID da sua organização"
    ["agent.custodian.id.extension"]="9911936"

    ["agent.custodian.id.root-text"]="Informe o root OID da sua organização"
    ["agent.custodian.id.root"]="2.16.840.1.113883.13.36"

    ["agent.organization.cnes-text"]="Indique o CNES da sua organização"
    ["agent.organization.cnes-comment"]="CNES do estabelecimento de saúde onde será instalado o agente agente (conector)"
    ["agent.organization.cnes"]=false

    ["agent.organization.cnesType-text"]="Indique o tipo de CNES da sua organização"
    ["agent.organization.cnesType-comment"]="Tipo de estabelecimento de saúde executando o agente conforme cadastro do CNES"
    ["agent.organization.cnesType"]="02"

    ["agent.organization.contactPerson-text"]="Indique o nome do responsável pela execução deste agente no seu estabelecimento"
    ["agent.organization.contactPerson-comment"]="Responsável pela execução do agente no estabelecimento de saúde"
    ["agent.organization.contactPerson"]=false

    ["agent.sourceId-text"]="Indique o OID do sistema integrado conforme cadastro na plataforma"
    ["agent.sourceId-comment"]="OID do sistema integrado conforme cadastro na paltaforma"
    ["agent.sourceId"]="1.3.6.1.4.1.54413.1.1.5.1"

    ["extractor.csv.folder-comment"]="Diretório para persistência dos extratos do banco de dados"
    ["extractor.csv.folder"]="csv"

    ["extractor.xml.folder-comment"]="Diretório para persistência dos arquivos de consolidação para transformação"
    ["extractor.xml.folder"]="xml"

    ["extractor.sql.minDate-text"]="Indique a data de início para extração no formato YYYY-MM-DD HH:mm:ss.sss"
    ["extractor.sql.minDate-default"]="2019-01-01 00:00:00.000"
    ["extractor.sql.minDate-comment"]="Intervalo de tempo para considerar na extração de RAC"
    ["extractor.sql.minDate"]=false

    ["extractor.sql.maxDate-text"]="Indique a data limite para extração no formato YYYY-MM-DD HH:mm:ss.sss"
    ["extractor.sql.maxDate-default"]="2100-01-01 00:00:00.000"
    ["extractor.sql.maxDate"]=false

    ["extractor.sql.periodPerBatch"]="PT1H"    
    
    ["agent.credentials.token.endpoint"]="https://ipes.tech:8243/token"

    ["agent.credentials.token"]=""

    ["agent.credentials.clientId-text"]="Indique o consumer key de acesso para a aplicação"
    ["agent.credentials.clientId-comment"]="Credenciais para o consumo dos serviços na plataforma"
    ["agent.credentials.clientId"]=false

    ["agent.credentials.clientSecret-text"]="Indique o consumer secret de acesso para a aplicação"
    ["agent.credentials.clientSecret"]=false

    ["jdbc.url-text"]="Indique a url JDBC de conexão com o banco de dados"
    ["jdbc.url-comment"]="Parâmetros de conexão com o banco de dados"
    ["jdbc.url"]=false

    ["jdbc.user-text"]="Indique o usuário do banco de dados"
    ["jdbc.user"]=false

    ["jdbc.password-text"]="Indique a senha do usuário do banco de dados"
    ["jdbc.password"]=false

    ["model.patient.endpoint-text"]="Indique o endpoint para o serviço de inserção de pacientes"
    ["model.patient.endpoint-comment"]="Endpoint dos serviços na plataforma"
    ["model.patient.endpoint"]="https://ipes.tech:8243/res/ihe/pix/manager/v3"

    ["model.practitioner.endpoint-text"]="Indique o endpoint para o serviço de inserção de profissionais de saúde"
    ["model.practitioner.endpoint"]="https://ipes.tech:8243/res/ihe/pix/manager/v3"

    ["model.bundle.endpoint-text"]="Indique o endpoint para o serviço de inserção de registros clínicos"
    ["model.bundle.endpoint"]="https://ipes.tech:8243/mhd/fhir/R4"

    ["management.endpoint.config"]="https://ipes.tech:8243/connector/{cnes}/instance/{sequential}/config"
    
    ["management.endpoint.status"]="https://ipes.tech:8243/connector/{cnes}/instance/{sequential}/status"
    
    ["extractor.cds.guardPeriod-comment"]="Período de guarda anterior a data atual para atrasar o envio de CDS"
    ["extractor.cds.guardPeriod"]="P2D"

    ["scheduler.delay.milliseconds-text"]="Indique o intervalo entre os processamentos"
    ["scheduler.delay.milliseconds-default"]="10000"
    ["scheduler.delay.milliseconds"]="10000"

    ["spring.beans.location"]=false
    ["spring.beans.location-text"]="Indique o tipo de conector a ser instalado: (1) PEP v3; (2) PEP v4; (3) Centralizador, (4) GSEA,(5) SISUPA"
    ["spring.beans.location-comment"]="Arquivo de carregamento de beans do Spring para provisionamento dinâmico de módulos"

    ["spring.main.allow-bean-definition-overriding-comment"]="Permissão para sobrescrever beans padrões do Spring Batch"
    ["spring.main.allow-bean-definition-overriding"]=true

    ["scheduler.practitioner.milliseconds-default"]="2419200000"
    ["scheduler.practitioner.milliseconds-comment"]="Tempo entre execuções da rotina de processamento em lote para Practitioner"
    ["scheduler.practitioner.milliseconds"]="2419200000"

    ["agent.credentials.aws.clientKey-text"]="Indique o client-key de acesso ao bucket da amazon"
    ["agent.credentials.aws.clientKey-comment"]="Chave e segredo do cliente da amazon com acesso ao buckets3"
    ["agent.credentials.aws.clientKey"]=false

    ["agent.credentials.aws.clientSecret-text"]="Indique o client-secret de acesso ao bucket da amazon"
    ["agent.credentials.aws.clientSecret"]=false
    
    
    ["agent.credentials.aws.bucketName-comment"]="Nome do bucket para envio de logs para auditoria e healthcheck"
    ["agent.credentials.aws.bucketName"]=conectoraudit01

    ["management.delay.milliseconds"]=3600000

    ["agent.custodian.id.sequencial-text"]="Indique um numero identificador único do conector a ser instalado na máquina local"
    ["agent.custodian.id.sequencial-default"]=1
    ["agent.custodian.id.sequencial-comment"]="Número sequencial identificador da instância do connector em uma máquina"
    ["agent.custodian.id.sequencial"]=false
    
    ["model.cds.municipio.ibgeNotIn-text"]="Indique os códigos de IBGE dos municípios que não se deseja capturar registros separados por vírgula"
    ["model.cds.municipio.ibgeNotIn-default"]=""
    ["model.cds.municipio.ibgeNotIn-comment"]="Códigos IBGE para serem excluídos nas queries de registros eletrônicos"
    ["model.cds.municipio.ibgeNotIn"]=false

    ["memory.gc.milliseconds-comment"]="Monitoramento e otimização do uso de memória"
    ["memory.gc.milliseconds"]=600000

    ["spring.build.version-comment"]="Propriedades da build"
    ["spring.build.version"]="@project.version@"

    ["scheduler.cron-text"]="Indique o horário de extração e envio de dados pelo agente com sintaxe cron"
    ["scheduler.cron-default"]="* * 0-23 ? * SUN-SAT"
    ["scheduler.cron-comment"]="Cron para validação do horário de atividade de extração"
    ["scheduler.cron"]=false
)

properties_order=(
    "agent.organization.contactPerson"
    "agent.organization.cnes"
    "agent.custodian.name"
    "agent.organization.cnesType"
    "agent.custodian.id.extension"
    "agent.custodian.id.root"
    "agent.sourceId"
    "extractor.cds.guardPeriod"
    "jdbc.url"
    "jdbc.user"
    "jdbc.password"
    "agent.custodian.name"
    "extractor.sql.minDate"
    "extractor.sql.maxDate"
    "agent.credentials.clientId"
    "agent.credentials.clientSecret"
    "agent.credentials.token"
    "agent.credentials.aws.clientKey"
    "agent.credentials.aws.clientSecret"
    "scheduler.delay.milliseconds"
    "scheduler.practitioner.milliseconds"
    "extractor.csv.folder"
    "extractor.xml.folder"
    "extractor.sql.periodPerBatch"
    "model.patient.endpoint"
    "model.practitioner.endpoint"
    "model.bundle.endpoint"
    "management.endpoint.config"
    "management.endpoint.status"
    "agent.credentials.token.endpoint"
    "spring.beans.location"
    "spring.main.allow-bean-definition-overriding"
    "agent.custodian.id.sequencial"
    "agent.credentials.aws.bucketName"
    "management.delay.milliseconds"
    "model.cds.municipio.ibgeNotIn"
    "memory.gc.milliseconds"
    "spring.build.version"
    "scheduler.cron"
)

# Obtenção do parâmetro que indica o arquivo de configuração
while :; do
    case $1 in
        -p=*|--properties_file=*)
            properties_file="${1#*=}"
            ;;
        -p|--properties_file)
            properties_file=$2
            ;;
        -j|--jar)
            jar_file=$2
            ;;
        -j=*|--jar=*)
            jar_file="${1#*=}"
            ;;
        *) break
    esac
    shift
    if [ "$1" != "" ]; then
        shift
    fi
done


# Nome padrão do arquivo de configuração é configurado caso não seja indicado
if [[ $properties_file == "" ]]; then
    properties_file="application.properties"
else
    printf "Obtendo configurações no arquivo $properties_file..\n\n"
fi


if [[ $jar_file == "" ]]; then
    printf "Pacote JAR não informado\nInforme a localização do pacote pelo parâmetro j / jar\n\n"
    exit 1
fi

if [ ! -f "$jar_file" ]; then
    echo "Pacote JAR informado não encontrado"
    exit 1
fi
    

ask_values=false

# Verifica-se se o arquivo de configuração existe
# Caso não exista, 
if [ ! -f "$properties_file" ]; then
    echo "Arquivo de configuração não encontrado"
    ask_values=true
else
    get_values_from_file
    for key in "${properties_order[@]}";
    do
        keyval=$key"-text"
        if [[ ${properties["$key"]} == false ]]; then
            echo "Arquivo não contém todas as informações necessárias"
            ask_values=true
            break
        fi
    done
fi

# Obtem-se os valores fantantes do usuário e insere valores padrões não especificados
# no arquivo de configuração indicado/criado
get_values_from_user

# Verificação de conexão com token de acesso
# response=$(curl -X POST -d "<conn_test></conn_test>" -H "Content-Type: application/xml" -H "Authorization: Bearer ${properties["agent.credentials.token"]}" --write-out %{http_code} --silent --output /dev/null ${properties["model.patient.endpoint"]})

# if [[ $response < 200 || $response > 299 ]]; then
#     echo "Houve um erro de conexão com a credencial informada."
#     echo "Por favor, verifique o valor de token informado."
#     echo 
#     echo "Todas as informações já preenchidas e necessárias para a execução do programa estão no arquivo '$properties_file'"
#     echo "É possível editá-lo e rodar o script de instalação com o parâmetro -p / --properties_file apontado para ele (campo 'agent.credentials.token')"
#     exit 1
# fi

command_not_found_handle() {
    exec >/dev/tty
    printf "Não foi possível verificar a integridade do arquivo jar '$jar_file'. Deseja continuar a instalação? [s/N]  "
    option='N'
    read option

    if [[ $option != 's' ]]; then
        return 127
    fi
    return 0
}

# Verifica-se a integridade do arquivo .jar informado
jarsigner -verify ${jar_file} >/dev/null
exit_status=$?

if [[ $exit_status = 1 ]]; then
    echo "Arquivo jar '$jar_file' não é um arquivo válido";
    echo "Encerrado a instalação...";
    exit 1
fi
if [[ $exit_status != 0 ]]; then
    echo "Encerrado a instalação...";
    exit 1
fi

command_not_found_handle() {
    exec >/dev/tty
    printf "Não foi possível verificar a conexão com o banco de dados pela falta de um cliente psql instalado no sistema.\r\nDeseja continuar a instalação? [s/N]  "
    option='N'
    read option

    if [[ $option != 's' ]]; then
        return 127
    fi
    return 1
}

conn_url=''
re='(^jdbc:)(.*)'

if [[ ${properties["jdbc.url"]} =~ $re ]]; then
    conn_url=${BASH_REMATCH[2]}
fi

re='(^postgresql://)(.*)'
if [[ $conn_url =~ $re ]]; then
    conn_url=${BASH_REMATCH[1]}${properties["jdbc.user"]}:${properties["jdbc.password"]}@${BASH_REMATCH[2]}
fi



pg_isready -d $conn_url >/dev/null 2>&1
connection_status=$?

if [[ $connection_status = 127 ]]; then
    echo "Encerrado a instalação...";
    exit 1
fi

if [[ $connection_status != 0 && $connection_status != 1 ]]; then
    echo "Não foi possível se conectar com o banco de dados pela jdbc_url informada (${properties["jdbc.url"]})";
    interrupt_installation
fi

if [[ $connection_status = 0 ]]; then
    psql -d $conn_url -c "SELECT now()" >/dev/null 2>&1
    credentials_status=$?
    if [[ $credentials_status = 127 ]]; then
        exit 1
    fi
    if [[ $credentials_status != 0 ]]; then
        echo "Não foi possível se conectar com o banco de dados pelas credenciais informadas (jdbc.user e jdbc.password)";
        interrupt_installation
    fi
fi

unset command_not_found_handle
set -e

SERVICE_NAME="$SERVICE_NAME-${properties["agent.organization.cnes"]}-${properties["agent.custodian.id.sequencial"]}"
SERVICE_DESTINATION="$SERVICE_DESTINATION-${properties["agent.organization.cnes"]}-${properties["agent.custodian.id.sequencial"]}"


systemctl is-active --quiet $SERVICE_NAME && sudo systemctl stop $SERVICE_NAME

[[ -d $SERVICE_DESTINATION ]] || sudo mkdir -p $SERVICE_DESTINATION

# Copia-se o arquivo de configuração do usuário completo para a pasta de instalação
sudo cp $properties_file $SERVICE_DESTINATION/connector.properties
sudo cp $jar_file $SERVICE_DESTINATION/connector.jar


sudo truncate -s 0 /etc/systemd/system/$SERVICE_NAME.service
sudo tee -a /etc/systemd/system/$SERVICE_NAME.service > /dev/null <<EOT
[Unit]
Description=RES Connector service
# After=network.target
StartLimitIntervalSec=10

[Service]
Type=simple
Restart=on-failure
RestartSec=1
ExecStart=/bin/bash $SERVICE_DESTINATION/connector_service.sh

[Install]
WantedBy=multi-user.target
EOT

sudo truncate -s 0 $SERVICE_DESTINATION/connector_service.sh
sudo tee -a $SERVICE_DESTINATION/connector_service.sh > /dev/null <<EOT
cd $SERVICE_DESTINATION
java -Xmx2g -XX:+UseG1GC -XX:MinHeapFreeRatio=10 -XX:MaxHeapFreeRatio=30 -XX:+UseStringDeduplication -jar connector.jar --spring.config.location=connector.properties
EOT

sudo truncate -s 0 $SERVICE_DESTINATION/updater_checker.sh
sudo tee -a $SERVICE_DESTINATION/updater_checker.sh > /dev/null <<EOT
#!/bin/bash
cd $SERVICE_DESTINATION

update() {
    curl $updater_link --output updater_runner.sh -s
    sudo chmod 755 updater_runner.sh
    sudo ./updater_runner.sh
    sudo rm updater_runner.sh
}

sudo unzip -o -qq connector.jar 'META-INF/MANIFEST.MF'

while IFS= read -r line; do
        if [[ -z \$line ]]; then
            continue
        fi
        if [[ \${line%:*} == "Implementation-Version" ]]; then
            local_version=\${line#*:}
            break
        fi
    done < "META-INF/MANIFEST.MF"
local_version=\$(echo "\$local_version" | sed -r 's/[\r\n]//g')
local_version=\$(echo "\$local_version" | sed -r 's/\s//g')
local_version=\$(echo "\$local_version" | sed -r 's/\.//g')

current_version=\$(curl $version_link -s)
current_version=\$(echo "\$current_version" | sed -r 's/[^0-9.]//g')
current_version=\$(echo "\$current_version" | sed -r 's/\s//g')
current_version=\$(echo "\$current_version" | sed -r 's/\.//g')


if [[ \$current_version > \$local_version ]]; then
    update
fi

rm -r META-INF
EOT

sudo chmod 755 /etc/systemd/system/$SERVICE_NAME.service
sudo chmod 755 $SERVICE_DESTINATION/connector_service.sh
sudo chmod 755 $SERVICE_DESTINATION/updater_checker.sh

# Obtenção dos serviços atuais do crontab
sudo crontab -l > mycron_tmp
#echo new cron into cron file
echo "30 12 * * * $SERVICE_DESTINATION/updater_checker.sh" >> mycron_tmp
#install new cron file
sudo crontab mycron_tmp
sudo rm mycron_tmp

printf "\nHabilitando e iniciando o serviço $SERVICE_NAME...\n\n"

sudo systemctl enable $SERVICE_NAME
sudo systemctl start $SERVICE_NAME

printf "Serviço $SERVICE_NAME inicializado com sucesso\n\n"

exit 0
